package com.score.senz_health.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.score.senz_health.R;

import static android.R.attr.typeface;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link patient_prescription_details.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link patient_prescription_details#newInstance} factory method to
 * create an instance of this fragment.
 */
public class patient_prescription_details extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Typeface typeface;
    private TextView reportselect;
    private Button browsebtn;
    private Button savebtn;
    private Button deletebtn;


    public patient_prescription_details() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment patient_prescription_details.
     */
    // TODO: Rename and change types and number of parameters
    public static patient_prescription_details newInstance(String param1, String param2) {
        patient_prescription_details fragment = new patient_prescription_details();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GeosansLight.ttf");
        setupUiElements();

    }

    private void setupUiElements() {
        reportselect = (TextView) getActivity().findViewById(R.id.selecte_img);
        reportselect.setTypeface(typeface, Typeface.NORMAL);


        browsebtn = (Button) getActivity().findViewById(R.id.browse_btn);
        browsebtn.setTypeface(typeface, Typeface.NORMAL);


        savebtn = (Button) getActivity().findViewById(R.id.save_btn);
        savebtn.setTypeface(typeface, Typeface.NORMAL);

        deletebtn = (Button) getActivity().findViewById(R.id.delete_btn);
        deletebtn.setTypeface(typeface, Typeface.NORMAL);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        browsebtn = (Button) getActivity().findViewById(R.id.browse_btn);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_patient_prescription_details, container, false);
    }


}
