package com.score.senz_health.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;
import android.content.SharedPreferences;

import com.score.senz_health.R;
import com.score.senz_health.exceptions.NoUserException;
import com.score.senz_health.remote.SenzService;
import com.score.senz_health.utils.PreferenceUtils;

/**
 * Splash activity, send login query from here
 *
 *
 */
public class SplashActivity extends BaseActivity {
    private final int SPLASH_DISPLAY_LENGTH = 3000;
    private static final String TAG = SplashActivity.class.getName();

    private static final String G_TAG = "Global"; // For Android Monitor debug : Global Tag

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(Bundle icicle) {
        Log.d(G_TAG, "Starting Splash Activity");
        super.onCreate(icicle);
        setContentView(R.layout.splash_layout);
        startService();
        initNavigation();
        setupSplashText();

    }

    private void setupSplashText() {
        ((TextView) findViewById(R.id.splash_name)).setTypeface(typefaceThin, Typeface.BOLD);
    }

    private void startService() {
        Intent serviceIntent = new Intent(this, SenzService.class);
        startService(serviceIntent);
    }

    /**
     * Determine where to go from here
     */
    private void initNavigation() {
        // determine where to go
        // start service
        try {

            PreferenceUtils.getUser(this);
            //on success of getting a user
            //Checking shared preferences again separetely to collect the role
            SharedPreferences sharedPreferences = getSharedPreferences("MyRole", Context.MODE_PRIVATE);
            String role = sharedPreferences.getString("Role", null);
            Log.d(G_TAG, "Collected the shared preference");

            // have user, so move to home
            navigateToHome(role);
        } catch (NoUserException e) {
            Log.d(G_TAG, "No user from preferenceUtil");
            e.printStackTrace();
            navigateToSplash();
        }
    }

    /**
     * Switch to home activity
     * This method will be call after successful login
     */
    private void navigateToSplash() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                navigateRegistration();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void navigateRegistration() {
        // no user, so move to registration
        Intent intent = new Intent(this, RegistrationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
        finish();
    }

    /**
     * Switch to home activity
     * This method will be call after successful login
     */
    public void navigateToHome(String role) {
        //Edit here to change drawer Activities
        if(role == null) {
            //if no role is saved
            Log.d(G_TAG, "No role is saved jump to splash");
            navigateToSplash();
        }else if(role.equals("Patient")){
            //Navigate to patient drawer
            Log.d(G_TAG, "Opening the Patient Drawer");
            Intent intent = new Intent(this, DrawerActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            SplashActivity.this.finish();
        } else if(role.equals("Doctor")){
            //Navigate to doctor drawer
            Log.d(G_TAG, "Opening the Doctor Drawer");
            Intent intent = new Intent(this, DrawerActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            SplashActivity.this.finish();
        } else{
            //Navigate to Lab drawer
            Log.d(G_TAG, "Opening the Laboratory Drawer");
            Intent intent = new Intent(this, DrawerActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            SplashActivity.this.finish();
        }
    }
}