package com.score.senz_health.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;

import android.support.v4.content.Loader;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.score.senz_health.R;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link patient_profile.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link patient_profile#newInstance} factory method to
 * create an instance of this fragment.
 */
public class patient_profile extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private static int RESULT_LOAD_IMAGE = 1;
    FloatingActionButton buttonLoadImage;
    private ScrollView mScrollView;
    private LinearLayout mFormView;
    private static final String G_TAG = "Global"; // For Android Monitor debug : Global Tag
    String UserImagePath;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Typeface typeface;
    private TextView username;
    private TextView  userage;


    private static int sId = 0;

    private static int id() {
        return sId++;
    }


    public patient_profile() {
        // Required empty public constructor
    }



    public static patient_profile newInstance(String param1, String param2) {
        patient_profile fragment = new patient_profile();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GeosansLight.ttf");
        setupUiElements();

    }
    private void setupUiElements() {


        Log.d(G_TAG, "Setting User Profile ImagePath : " + UserImagePath);
        //getString(R.string.preference_file_key), Context.MODE_PRIVATE
        SharedPreferences sharedPreferences =getActivity().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        String usrprofname = sharedPreferences.getString("username", "default");
        Log.d(G_TAG, "Inside patient profile username : "+ usrprofname);
        username = (TextView) getActivity().findViewById(R.id.user_name);
        username.setText(usrprofname);
        username.setTypeface(typeface, Typeface.NORMAL);

        userage = (TextView) getActivity().findViewById(R.id.user_age);
        userage.setTypeface(typeface, Typeface.NORMAL);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);


    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        if (mScrollView == null) {
            // normally inflate the view hierarchy
            mScrollView = (ScrollView) inflater.inflate(R.layout.fragment_patient_profile,container, false);
            mFormView = (LinearLayout) mScrollView.findViewById(R.id.app_bar_layout);

            buttonLoadImage = (FloatingActionButton) mScrollView.findViewById(R.id.capture_selfie);

            buttonLoadImage.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {

                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(i, RESULT_LOAD_IMAGE);
                }
            });


        } else {

            ViewGroup parent = (ViewGroup) mScrollView.getParent();
            parent.removeView(mScrollView);
        }
        return mScrollView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE  && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            SharedPreferences preferenceUProf = getActivity().getSharedPreferences("SenzUser", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferenceUProf.edit();
            cursor.close();
            editor.putString("UIPath", picturePath);
            editor.commit();
            UserImagePath = preferenceUProf.getString("UIPath", "No Path Yet");
            try {
                Log.d(G_TAG, "SHared Pref UPImage : "+UserImagePath);
                Log.d(G_TAG, "picturepath : "+picturePath);
                if(!(UserImagePath.equals("No Path Yet"))) {
                    ImageView imageView = (ImageView) mFormView.findViewById(R.id.imgv);
                    imageView.setImageBitmap(BitmapFactory.decodeFile(UserImagePath));
                }


            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }



    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }




    public void onLoadFinished(Loader<Void> id, Void result) {

    }

    public void onLoaderReset(Loader<Void> loader) {
    }
}
