package com.score.senz_health.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.score.senz_health.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link patient_get_appoinment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link patient_get_appoinment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class patient_get_appoinment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private Typeface typeface;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private TextView  docname;
    private TextView  locname;
    private Button docbtn;
    private Button locbtn;
    private EditText  locedit;
    private TextView  docedit;


    public patient_get_appoinment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment patient_get_appoinment.
     */
    // TODO: Rename and change types and number of parameters
    public static patient_get_appoinment newInstance(String param1, String param2) {
        patient_get_appoinment fragment = new patient_get_appoinment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GeosansLight.ttf");
        setupUiElements();
    }
    private void setupUiElements() {
        docname = (TextView) getActivity().findViewById(R.id.doc_name);
        docname.setTypeface(typeface, Typeface.NORMAL);

        docbtn=(Button) getActivity().findViewById(R.id.doc_btn);
        docbtn.setTypeface(typeface, Typeface.NORMAL);

        docedit=(EditText) getActivity().findViewById(R.id.doc_edit);
        docedit.setTypeface(typeface, Typeface.NORMAL);

        locname = (TextView) getActivity().findViewById(R.id.loc_name);
        locname.setTypeface(typeface, Typeface.NORMAL);

        locbtn=(Button) getActivity().findViewById(R.id.btn_location);
       locbtn.setTypeface(typeface, Typeface.NORMAL);

        locedit=(EditText) getActivity().findViewById(R.id.loc_edit);
        locedit.setTypeface(typeface, Typeface.NORMAL);




    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_patient_get_appoinment, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event






}
