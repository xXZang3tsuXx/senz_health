package com.score.senz_health.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.score.senz_health.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link patient_manage_report.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link patient_manage_report#newInstance} factory method to
 * create an instance of this fragment.
 */
public class patient_manage_report extends Fragment implements Button.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Typeface typeface;
    private TextView selectReport ;
    private ImageView imgReport;
    private Button btnView;
    private Button btnSend;
    private Button btndelete;



    public patient_manage_report() {
        // Required empty public constructor
    }



    private static int RESULT_LOAD_IMAGE = 1;
    public static patient_manage_report newInstance(String param1, String param2) {
        patient_manage_report fragment = new patient_manage_report();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_patient_manage_report, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GeosansLight.ttf");
        setupUiElements();
    }
    private void setupUiElements() {
        selectReport = (TextView) getActivity().findViewById(R.id.txt_select_report);
                selectReport.setTypeface(typeface, Typeface.NORMAL);


        btnView=(Button) getActivity().findViewById(R.id.btn_view);
        btnView.setTypeface(typeface, Typeface.NORMAL);


        btnSend=(Button) getActivity().findViewById(R.id.btn_send);
        btnSend.setTypeface(typeface, Typeface.NORMAL);

        btndelete=(Button) getActivity().findViewById(R.id.btn_delete);
        btndelete.setTypeface(typeface, Typeface.NORMAL);


    }


    @Override
    public void onClick(View view) {

    }
}
