package com.score.senz_health.interfaces;

import com.score.senz_health.pojo.Contact;

import java.util.ArrayList;

/**
 * Created by eranga on 12/10/16.
 */

public interface IContactReaderListener {
    void onPostRead(ArrayList<Contact> contactList);
}
